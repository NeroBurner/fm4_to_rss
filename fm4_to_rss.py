import argparse
import requests
import re
import time
import os
import sys
import locale
from datetime import datetime
# generate html
import mako.exceptions
from mako.template import Template

def parse_entry(list_entry, referrer="", verbose=False):
    if verbose:
        print("parse_entry: begin")
    entry = dict()

    title = re.search('(?:<h2><span>)(.*?)(?:</span></h2>)', list_entry)[1]
    description = title
    link = re.search('(?:<a href=")([^"]*)', list_entry)[1]

    author = re.search('(?:<p class="keyword">)(.*?)(?:</p>)', list_entry)[1]
    guid = re.search('(?:https://fm4.orf.at/stories/)(.*?)(?:/)', link)[1]
    pubDateIn = re.search('(?:<p class="time hidden-md">)(.*?)(?:</p>)', list_entry)[1]

    # in  07. April 2019
    # out Sun, 7 Apr 2019 00:00:00 +0000
    locale.setlocale(locale.LC_TIME, 'de_AT.UTF-8')
    last_time_updated = datetime.strptime(pubDateIn, '%d. %B %Y')
    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    pubDate = last_time_updated.strftime('%a, %d %b %Y 00:00:00 +0000')

    entry = {
            "title": title,
            "description": description,
            "link": referrer + link,
            "author": author,
            "guid": guid,
            "pubDate": pubDate,
            }
    if verbose:
        print("parse_entry: found entry with title: '{}'".format(title))
    return entry

def parse_site(url, referrer="", verbose=False):
    if verbose:
        print("parse_site: parsing url: '{}'".format(url))
    r = requests.get(url)
    html = r.text

    re_list_elements = '(?:<div class="listItem col-lg-2 col-sm-1 image">)(.*?)(?:</div>)'
    matches = re.findall(re_list_elements, html, re.DOTALL)

    if verbose:
        print("parse_site: number of matches found in site: {}".format(len(matches)))
    if len(matches) == 0:
        raise ValueError("found no entries for site: '{}'".format(url))

    data = [parse_entry(x, referrer, verbose) for x in matches]

    if verbose:
        print("parse_site: generated data")
    return data

def generate_rss(channel, data, output, verbose=False):
    template_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "rss.mako")
    with open(output, "wb") as f:
        try:
            reportTemplate = Template(filename=template_path, input_encoding='utf-8', output_encoding='utf-8')
            f.write(reportTemplate.render(
                    channel=channel,
                    data=data
            ))
        except Exception:
            print(mako.exceptions.text_error_template().render())
            sys.exit(1)

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--url",
                        help="URL to FM4 tag.",
                        default="https://fm4.orf.at/tags/")
    parser.add_argument("--tag",
                        help="URL to FM4 tag.",
                        default="erichmoechel")
    parser.add_argument("--referrer",
                        help="referrer URL to prepend to the article URL.",
                        default="")
    parser.add_argument("-o", "--output",
                        help="RSS output file",
                        default="out.rss")
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()

    if args.verbose:
        print("verbosity turned on")

    all_start = time.time()

    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    channel_pub_date = datetime.now().strftime('%a, %d %b %Y %H:%M:%S +0000')

    link = args.url + args.tag
    channel = {
            "title": "FM4 " + args.tag,
            "link": link,
            "description": "FM4 " + args.tag,
            "language": "de-at",
            "copyright": "ORF Online und Teletext GmbH und Co KG",
            "pubDate": channel_pub_date,
            "image": {
                    "url": "https://tubestatic.orf.at/mojo/1_3/storyserver/tube/fm4/images/fm4.logo.svg",
                    "title": "FM4",
                    "link": args.url,
                    },
            }
    data = parse_site(link, args.referrer, args.verbose)
    generate_rss(channel, data, args.output, args.verbose)

    print("total runtime {:.3f} s".format(time.time() - all_start))

if __name__ == "__main__":
    main()

